-- Gerado por Oracle SQL Developer Data Modeler 19.4.0.350.1424
--   em:        2020-10-23 22:01:16 BRT
--   site:      Oracle Database 11g
--   tipo:      Oracle Database 11g



CREATE TABLE eventos (
    eve_id               NUMBER(6) NOT NULL,
    eve_nome             VARCHAR2(100) NOT NULL,
    eve_data             DATE NOT NULL,
    eve_valor            NUMBER(10, 2) NOT NULL,
    eve_data_pagamento   DATE NOT NULL,
    eve_data_vencimento  DATE NOT NULL,
    eve_tev_id           NUMBER(6) NOT NULL,
    eve_loc_id           NUMBER(6) NOT NULL,
    eve_sta_id           NUMBER(6) NOT NULL,
    eve_res_id           NUMBER(6) NOT NULL,
    eve_tpa_id           NUMBER(6) NOT NULL
);

ALTER TABLE eventos ADD CONSTRAINT pk_eve PRIMARY KEY ( eve_id );

CREATE TABLE locais_eventos (
    loc_id          NUMBER(6) NOT NULL,
    loc_numero      VARCHAR2(100) NOT NULL,
    loc_cidade      VARCHAR2(100) NOT NULL,
    loc_logradouro  VARCHAR2(100) NOT NULL,
    loc_cep         VARCHAR2(100) NOT NULL,
    loc_estado      VARCHAR2(100) NOT NULL,
    loc_bairro      VARCHAR2(100) NOT NULL
);

ALTER TABLE locais_eventos ADD CONSTRAINT pk_loc PRIMARY KEY ( loc_id );

CREATE TABLE responsaveis (
    res_id              NUMBER(6) NOT NULL,
    res_nome            VARCHAR2(100) NOT NULL,
    res_sobrenome       VARCHAR2(100) NOT NULL,
    res_telefone_fixo   VARCHAR2(100),
    res_telefone_movel  VARCHAR2(100),
    res_email           VARCHAR2(100) NOT NULL
);

ALTER TABLE responsaveis ADD CONSTRAINT pk_res PRIMARY KEY ( res_id );

CREATE TABLE status_agendamento (
    sta_id      NUMBER(6) NOT NULL,
    sta_status  VARCHAR2(100) NOT NULL
);

ALTER TABLE status_agendamento ADD CONSTRAINT pk_sta PRIMARY KEY ( sta_id );

CREATE TABLE tipos_eventos (
    tev_id         NUMBER(6) NOT NULL,
    tev_descricao  VARCHAR2(100)
);

ALTER TABLE tipos_eventos ADD CONSTRAINT pk_tev PRIMARY KEY ( tev_id );

CREATE TABLE tipos_pagamentos (
    tpa_id    NUMBER(6) NOT NULL,
    tpa_tipo  VARCHAR2(100)
);

ALTER TABLE tipos_pagamentos ADD CONSTRAINT pk_tpa PRIMARY KEY ( tpa_id );

ALTER TABLE eventos
    ADD CONSTRAINT fk_eve_loc FOREIGN KEY ( eve_loc_id )
        REFERENCES locais_eventos ( loc_id );

ALTER TABLE eventos
    ADD CONSTRAINT fk_eve_res FOREIGN KEY ( eve_res_id )
        REFERENCES responsaveis ( res_id );

ALTER TABLE eventos
    ADD CONSTRAINT fk_eve_sta FOREIGN KEY ( eve_sta_id )
        REFERENCES status_agendamento ( sta_id );

ALTER TABLE eventos
    ADD CONSTRAINT fk_eve_tev FOREIGN KEY ( eve_tev_id )
        REFERENCES tipos_eventos ( tev_id );

ALTER TABLE eventos
    ADD CONSTRAINT fk_eve_tpa FOREIGN KEY ( eve_tpa_id )
        REFERENCES tipos_pagamentos ( tpa_id );



-- Relatório do Resumo do Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                             6
-- CREATE INDEX                             0
-- ALTER TABLE                             11
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   0
-- WARNINGS                                 0
