--TIPOS_PAGAMENTOS
insert into tipos_pagamentos (tpa_tipo) VALUES ('DEBITO');
insert into tipos_pagamentos (tpa_tipo) VALUES ('CREDITO');
insert into tipos_pagamentos (tpa_tipo) VALUES ('DINHEIRO');

--TIPOS_EVENTOS
insert into tipos_eventos (tev_descricao) VALUES ('SHOW');
insert into tipos_eventos (tev_descricao) VALUES ('STAND-UP');
insert into tipos_eventos (tev_descricao) VALUES ('CASAMENTO');
insert into tipos_eventos (tev_descricao) VALUES ('ANIVERSÁRIO');
insert into tipos_eventos (tev_descricao) VALUES ('PALESTRA');
insert into tipos_eventos (tev_descricao) VALUES ('APRESENTAÇÃO');
insert into tipos_eventos (tev_descricao) VALUES ('COLAÇÃO DE GRAU');
insert into tipos_eventos (tev_descricao) VALUES ('EVENTOS RELIGIOSOS');
insert into tipos_eventos (tev_descricao) VALUES ('TREINAMENTO');
insert into tipos_eventos (tev_descricao) VALUES ('ENCONTRO ESPIRITUAL');
insert into tipos_eventos (tev_descricao) VALUES ('CONFERÊNCIA');
insert into tipos_eventos (tev_descricao) VALUES ('HAPPY HOUR');
insert into tipos_eventos (tev_descricao) VALUES ('FORMATURA');

--STATUS_AGENDAMENTO
insert into status_agendamento (sta_status) VALUES ('AGENDADO');
insert into status_agendamento (sta_status) VALUES ('PENDETE');
insert into status_agendamento (sta_status) VALUES ('CONCLUIDO');
insert into status_agendamento (sta_status) VALUES ('CANCELADO');
insert into status_agendamento (sta_status) VALUES ('AGUARDANDO PAGAMENTO');

--RESPONSAVEIS
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Wylma', 'Akess', '(20)6742-6896', '(37)79935-1634', 'wakess0@wordpress.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Alexine', 'Skepper', '(47)8518-6727', '(81)80573-7871', 'askepper1@archive.org');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Babbie', 'Seabon', '(85)9252-4376', '(19)84809-4419', 'bseabon2@twitpic.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Crichton', 'Mayling', '(69)3641-1127', '(97)02709-8121', 'cmayling3@mapy.cz');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Antonetta', 'Conn', '(48)9034-3347', '(88)99018-0562', 'aconn4@discovery.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Carrol', 'Pettman', '(13)7180-5977', '(25)30692-5449', 'cpettman5@a8.net');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Avery', 'Mesant', '(31)9334-1299', '(17)92702-4560', 'amesant6@mapquest.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Eddy', 'D''Acth', '(48)9550-3597', '(76)16129-1528', 'edacth7@altervista.org');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Trip', 'Brumbie', '(21)2612-4886', '(47)82164-7550', 'tbrumbie8@hc360.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Fredia', 'Osgarby', '(86)8826-0234', '(85)05988-0409', 'fosgarby9@infoseek.co.jp');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Arlen', 'Valentinuzzi', '(08)6009-0491', '(27)69949-5482', 'avalentinuzzia@mtv.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Nefen', 'Eteen', '(39)8669-4185', '(08)70000-2906', 'neteenb@intel.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Valery', 'Stonebridge', '(12)8179-6918', '(42)91900-8096', 'vstonebridgec@blogspot.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Lester', 'Briatt', '(08)3980-6879', '(56)09399-3311', 'lbriattd@bloglines.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Drake', 'Aslum', '(51)3209-0659', '(36)72516-2395', 'daslume@hostgator.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Corny', 'Tilbey', '(50)4438-1904', '(91)97981-9610', 'ctilbeyf@shutterfly.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Aleda', 'Boothman', '(93)6994-7946', '(80)21387-6950', 'aboothmang@uiuc.edu');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Row', 'Astling', '(57)0266-7482', '(22)17278-1745', 'rastlingh@ning.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Valery', 'Bouchier', '(11)9348-5627', '(78)90431-0688', 'vbouchieri@ebay.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Carlynne', 'Yesinov', '(28)5365-3643', '(59)63291-1940', 'cyesinovj@state.tx.us');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Germana', 'Padbury', '(30)1791-6522', '(47)08937-0748', 'gpadburyk@archive.org');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Yale', 'Ivimey', '(68)7226-3948', '(65)69733-3802', 'yivimeyl@mit.edu');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Katha', 'Gawkroge', '(22)3301-6977', '(45)49079-8180', 'kgawkrogem@unc.edu');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Kienan', 'Waszczyk', '(61)4361-8143', '(25)90693-5471', 'kwaszczykn@vk.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Cello', 'Attkins', '(48)2958-0789', '(25)07083-0329', 'cattkinso@youku.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Harmon', 'Whetland', '(95)4570-0878', '(77)41835-7415', 'hwhetlandp@so-net.ne.jp');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Cristobal', 'Gun', '(49)4588-4701', '(77)83944-4295', 'cgunq@xinhuanet.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Garik', 'Bellows', '(93)5795-6791', '(01)16323-5740', 'gbellowsr@economist.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Kayne', 'Huddlestone', '(63)7514-8778', '(39)82778-8474', 'khuddlestones@youku.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Annabell', 'Gyrgorwicx', '(99)8137-6107', '(85)32566-5159', 'agyrgorwicxt@who.int');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Raven', 'Cancott', '(95)1408-1158', '(22)88419-8065', 'rcancottu@storify.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Tobin', 'Faulder', '(76)7540-0947', '(34)51965-6926', 'tfaulderv@yale.edu');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Lanae', 'Sturch', '(31)9677-3852', '(45)65014-7157', 'lsturchw@ow.ly');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Ardene', 'Vanderplas', '(14)0946-7840', '(72)44660-2138', 'avanderplasx@wp.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Aldin', 'Mobbs', '(85)7794-1482', '(26)21193-2049', 'amobbsy@salon.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Gilberta', 'Garroway', '(02)9240-9662', '(87)62441-7068', 'ggarrowayz@redcross.org');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Warren', 'Atthow', '(43)2664-1647', '(14)95164-4720', 'watthow10@shutterfly.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Jeannette', 'Waistell', '(32)0368-0516', '(37)09278-3694', 'jwaistell11@intel.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Carol', 'Guage', '(98)1149-8140', '(00)62752-1549', 'cguage12@businessweek.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Jozef', 'Curro', '(85)3813-9071', '(00)36940-6416', 'jcurro13@ameblo.jp');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Amelia', 'Manifould', '(86)7268-1919', '(54)01763-2957', 'amanifould14@dedecms.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Jerrie', 'Jack', '(09)0336-0594', '(60)19705-5692', 'jjack15@howstuffworks.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Angie', 'Dryden', '(92)4720-9573', '(85)01333-9260', 'adryden16@skyrock.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Sacha', 'Hoofe', '(70)6787-7660', '(06)79992-9574', 'shoofe17@ihg.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Prudy', 'Alekseicik', '(76)5957-0979', '(85)89146-1714', 'palekseicik18@tiny.cc');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Analise', 'Sawrey', '(15)4762-2671', '(00)18512-8934', 'asawrey19@meetup.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Alexia', 'Rosgen', '(53)5626-8755', '(96)81645-3182', 'arosgen1a@booking.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Doug', 'Walentynowicz', '(21)3175-5788', '(93)58475-6870', 'dwalentynowicz1b@nsw.gov.au');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Beauregard', 'Pickvance', '(27)5207-1933', '(70)99083-3947', 'bpickvance1c@mashable.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Aldric', 'Bon', '(14)0145-8843', '(51)25265-6694', 'abon1d@lulu.com');

--LOCAIS_EVENTO
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3360', 'Zheleznogorsk', 'Shoshone', '26875-056', 'PI', 'Way');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7280', 'Xinhuang', 'Dryden', '93945-850', 'GO', 'Point');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6467', 'Ḩarastā', '1st', '31966-976', 'CE', 'Center');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2070', 'Embalse', 'Larry', '20154-577', 'RO', 'Hill');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2995', 'Jiupu', 'Beilfuss', '35315-884', 'PR', 'Trail');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('8953', 'Bauchi', 'Sheridan', '07546-721', 'MA', 'Junction');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2346', 'Fort Dauphin', 'Village', '97232-130', 'ES', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3738', 'Berezne', 'Trailsway', '86927-563', 'RJ', 'Trail');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2073', 'Chambishi', 'Golden Leaf', '38719-079', 'MT', 'Hill');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('5929', 'Gustavia', 'Pierstorff', '03847-839', 'PA', 'Alley');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6404', 'Chaoyangdong', 'Morning', '30733-037', 'MG', 'Terrace');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6398', 'Cihaladan', 'Park Meadow', '10603-194', 'GO', 'Street');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7353', 'Odintsovo', 'Surrey', '46072-424', 'TO', 'Pass');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7165', 'Cidade Velha', 'Fieldstone', '31315-109', 'MS', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('5733', 'Kalá Déndra', 'Granby', '30484-499', 'RO', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7206', 'Xinmatou', 'Boyd', '31313-430', 'GO', 'Park');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('5589', 'Fengle', 'Brown', '38181-253', 'RS', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('8126', 'Prusice', 'Mifflin', '98880-253', 'RR', 'Park');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('9699', 'Korogan Timur', 'Carpenter', '57971-616', ' AL', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('9729', 'San Cristóbal', 'Blue Bill Park', '67640-261', 'PI', 'Crossing');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('0173', 'Jiangkou', 'Ridgeway', '48799-121', ' AC', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('0578', 'Yonezawa', 'Susan', '63948-569', ' AC', 'Center');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7993', 'Lanot', 'Chinook', '08302-856', 'AM', 'Hill');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3037', 'Alcains', 'Nevada', '40778-104', 'MT', 'Way');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7140', 'São Gonçalo do Amarante', 'Commercial', '62204-742', 'MA', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7260', 'Sandefjord', 'Forest', '18628-179', 'PB', 'Terrace');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7961', 'West End', 'Prentice', '97867-543', 'GO', 'Circle');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('5279', 'Moengo', 'Straubel', '04149-049', 'ES', 'Drive');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2498', 'El Matama', 'Petterle', '99418-810', 'PR', 'Circle');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('9467', 'Péfki', 'Hauk', '08363-111', 'ES', 'Hill');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6155', 'Huangdimiao', 'Kennedy', '62198-412', 'MT', 'Alley');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('0954', 'Sambiyan', 'Orin', '40818-093', 'PE', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6176', 'Wanmao', 'Stephen', '81198-747', ' AC', 'Court');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('8064', 'Lerrnanist', 'Morningstar', '46980-341', 'PR', 'Court');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('0152', 'Merritt', 'Algoma', '30581-701', 'MA', 'Junction');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3689', 'Dapaong', 'Northview', '10803-832', 'RR', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('5923', 'Gielniów', 'Del Sol', '89049-783', 'MS', 'Junction');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('4592', 'Valuyki', 'Buhler', '60633-303', 'ES', 'Drive');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7696', 'Asen', 'Prairie Rose', '35376-553', 'TO', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6504', 'Rouen', 'Westridge', '64504-628', 'PB', 'Court');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('1079', 'Aţ Ţaybah', 'Leroy', '47057-750', 'BA', 'Junction');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3325', 'Chang Klang', 'Claremont', '02756-280', 'MT', 'Point');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('1733', 'Gouménissa', 'Anniversary', '24216-532', 'PE', 'Drive');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2333', 'Cane', 'Columbus', '94301-567', 'RJ', 'Hill');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('8683', 'Margahayu', 'Summer Ridge', '55699-496', 'SC', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7862', 'Mwene-Ditu', 'Debra', '57054-294', 'AM', 'Trail');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6223', 'Bukowina Tatrzańska', 'Lukken', '10882-793', 'PR', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3157', 'La Esmeralda', 'Bonner', '94013-414', 'ES', 'Street');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7223', 'San Kamphaeng', 'Basil', '93810-549', 'MS', 'Plaza');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('1673', 'Firovo', 'Grayhawk', '47653-501', 'ES', 'Trail');

-- --EVENTO
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Tresom', '01-09-2020', 5384.99, '21-08-2020', '28-08-2020', 1, 1, 1, 1, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Ventosanzap', '23/6/2020', '1682,32', '20/12/2019', '8/1/2020', 4, 12, 2, 45, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Voltsillam', '7/8/2020', '3717,46', '28/12/2019', '27/12/2019', 7, 5, 4, 34, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Tres-Zap', '18/6/2020', '3959,76', '15/9/2020', '1/7/2020', 13, 4, 4, 10, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Fix San', '2/10/2020', '2892,57', '1/9/2020', '23/12/2019', 3, 45, 2, 15, 3);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Voltsillam', '14/9/2020', '3842,37', '15/10/2020', '7/1/2020', 4, 14, 4, 34, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Bitwolf', '14/12/2019', '4269,01', '2/12/2019', '24/11/2019', 9, 31, 2, 1, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Voltsillam', '29/11/2019', '9307,46', '3/4/2020', '10/3/2020', 10, 14, 2, 17, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Toughjoyfax', '14/12/2019', '6351,61', '18/1/2020', '27/1/2020', 2, 12, 4, 34, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Bitwolf', '12/11/2019', '4921,51', '24/4/2020', '8/1/2020', 5, 11, 3, 41, 3);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Namfix', '13/1/2020', '5949,58', '12/6/2020', '3/2/2020', 7, 33, 2, 14, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Fintone', '6/3/2020', '6172,90', '19/4/2020', '20/12/2019', 13, 2, 4, 34, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Temp', '16/1/2020', '7101,45', '8/10/2020', '11/4/2020', 11, 11, 1, 19, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Redhold', '6/6/2020', '1351,06', '20/12/2019', '31/7/2020', 12, 42, 2, 19, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Tin', '2/5/2020', '2931,60', '7/2/2020', '4/6/2020', 6, 34, 4, 42, 3);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Span', '10/6/2020', '6443,35', '29/4/2020', '16/10/2020', 9, 1, 5, 1, 6);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Bitwolf', '30/3/2020', '3293,91', '23/6/2020', '25/8/2020', 2, 17, 2, 6, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Sonsing', '6/11/2019', '6863,48', '18/4/2020', '20/6/2020', 4, 29, 4, 46, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Ventosanzap', '22/11/2019', '0517,58', '7/12/2019', '23/6/2020', 5, 1, 1, 1, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Pannier', '4/7/2020', '3409,74', '2/6/2020', '2/10/2020', 9, 5, 2, 17, 3);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Transcof', '17/1/2020', '8473,90', '1/6/2020', '13/2/2020', 3, 25, 4, 42, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Ronstring', '2/5/2020', '7751,07', '19/5/2020', '3/7/2020', 4, 8, 3, 32, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Wrapsafe', '13/1/2020', '3754,93', '25/12/2019', '21/3/2020', 7, 17, 2, 17, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Cardguard', '14/9/2020', '6125,12', '5/4/2020', '4/5/2020', 10, 47, 4, 8, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Alpha', '29/7/2020', '3752,68', '4/7/2020', '3/6/2020', 3, 10, 2, 19, 3);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Ronstring', '24/6/2020', '7859,99', '2/8/2020', '20/2/2020', 4, 18, 2, 19, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Fixflex', '10/7/2020', '3028,92', '29/1/2020', '29/3/2020', 7, 49, 4, 7, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Mat Lam Tam', '21/7/2020', '6133,05', '21/6/2020', '10/10/2020', 10, 1, 1, 1, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Zontrax', '1/1/2020', '2276,88', '30/4/2020', '4/7/2020', 5, 15, 2, 16, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Cardify', '19/12/2019', '0757,06', '30/1/2020', '5/7/2020', 5, 16, 4, 10, 3);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Tampflex', '9/9/2020', '7946,02', '7/2/2020', '22/12/2019', 7, 8, 4, 17, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Tin', '4/12/2019', '1941,88', '25/12/2019', '22/2/2020', 4, 17, 2, 20, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Keylex', '19/9/2020', '2852,59', '10/4/2020', '5/8/2020', 12, 15, 4, 16, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Home Ing', '11/1/2020', '0500,94', '22/10/2020', '24/11/2019', 4, 42, 3, 13, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Namfix', '6/12/2019', '1514,31', '22/2/2020', '21/11/2019', 13, 49, 2, 39, 3);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Fix San', '8/3/2020', '4504,54', '23/9/2020', '19/2/2020', 11, 14, 4, 7, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Transcof', '11/12/2019', '6988,04', '30/4/2020', '28/8/2020', 2, 19, 1, 1, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Job', '12/8/2020', '4724,99', '10/8/2020', '4/3/2020', 4, 17, 2, 44, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Bitchip', '14/3/2020', '5369,46', '12/9/2020', '21/2/2020', 7, 18, 4, 8, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Home Ing', '10/7/2020', '1709,03', '26/3/2020', '25/7/2020', 10, 1, 1, 1, 3);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Tin', '9/6/2020', '3264,43', '7/3/2020', '19/4/2020', 1, 17, 2, 23, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Voyatouch', '1/3/2020', '7946,17', '2/5/2020', '5/11/2019', 4, 34, 4, 2, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Home Ing', '5/7/2020', '9644,75', '29/6/2020', '30/3/2020', 7, 3, 5, 12, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Cookley', '29/2/2020', '3347,86', '26/10/2019', '13/8/2020', 10, 21, 2, 8, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Subin', '11/6/2020', '8737,29', '19/4/2020', '28/12/2019', 1, 34, 4, 5, 3);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Ronstring', '29/12/2019', '1577,93', '13/4/2020', '15/5/2020', 4, 9, 4, 16, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Alpha', '23/11/2019', '1718,09', '24/11/2019', '27/5/2020', 7, 4, 2, 33, 1);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Job', '5/11/2019', '9632,00', '15/11/2019', '16/2/2020', 10, 34, 4, 43, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Cardify', '11/12/2019', '3101,43', '23/8/2020', '12/10/2020', 6, 8, 2, 12, 2);
insert into eventos (eve_nome, eve_data, eve_valor, eve_data_pagamento, eve_data_vencimento, eve_tev_id, eve_loc_id, eve_sta_id, eve_res_id, eve_tpa_id) values ('Regrant', '28/8/2020', '1782,96', '23/8/2020', '25/9/2020', 4, 6, 2, 5, 3);

--- *** EVENTOS ***
insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 4
  eve_sta_id,-- 1 a 4
  eve_res_id,-- 1 a 100
  eve_tpa_id, -- 1 a 2
  eve_loc_id
) values (
  'Show do Truman',
  TO_DATE('12-02-2020', 'DD-MM-YYYY HH24:MI'),
  7566.34,
  TO_DATE('12-02-2020', 'DD-MM-YYYY'),
  TO_DATE('12-02-2020', 'DD-MM-YYYY'),
  2, 
  1,
  3,
  1,
  1
);


insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 4
  eve_sta_id,-- 1 a 4
  eve_res_id,-- 1 a 100
  eve_tpa_id, -- 1 a 2
  eve_loc_id
) values (
  'Meetup do Nerdzão',
  TO_DATE('12-11-2020 23:55', 'DD-MM-YYYY HH24:MI'),
  36880.87,
  TO_DATE('12-11-2020', 'DD-MM-YYYY'),
  TO_DATE('12-12-2020', 'DD-MM-YYYY'),
  3, 
  1,
  3,
  1,
  1
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 4
  eve_sta_id,-- 1 a 4
  eve_res_id,-- 1 a 100
  eve_tpa_id, -- 1 a 2
  eve_loc_id
) values (
  'Meetup do Caquicoders',
  TO_DATE('08-09-2020 08:35', 'DD-MM-YYYY HH24:MI'),
  2737.65,
  TO_DATE('10-09-2020', 'DD-MM-YYYY'),
  TO_DATE('12-10-2020', 'DD-MM-YYYY'),
  6, 
  1,
  3,
  1,
  1
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 100
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Show Aerosmith',
  TO_DATE('30-10-2020 19:30', 'DD-MM-YYYY HH24:MI'),
  7642.56,
  TO_DATE('10-10-2020', 'DD-MM-YYYY'),
  TO_DATE('16-10-2020', 'DD-MM-YYYY'),
  1, 
  4,
  45,
  1,
  33
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 50
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Teatro do Fabio Porchat',
  TO_DATE('20-11-2020 20:30', 'DD-MM-YYYY HH24:MI'),
  1427.76,
  TO_DATE('12-11-2020', 'DD-MM-YYYY'),
  TO_DATE('13-12-2020', 'DD-MM-YYYY'),
  2, 
  1,
  6,
  2,
  23
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 50
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Casamento do Robinho',
  TO_DATE('25-12-2020 16:30', 'DD-MM-YYYY HH24:MI'),
  4667.76,
  TO_DATE('05-11-2020', 'DD-MM-YYYY'),
  TO_DATE('10-11-2020', 'DD-MM-YYYY'),
  3, 
  3,
  50,
  3,
  12
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 50
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Festa do Latino',
  TO_DATE('30-02-2021 22:00', 'DD-MM-YYYY HH24:MI'),
  13090.76,
  TO_DATE('10-01-2020', 'DD-MM-YYYY'),
  TO_DATE('30-01-2020', 'DD-MM-YYYY'),
  4, 
  4,
  26,
  2,
  8
);


insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 50
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Fim de semana no parque',
  TO_DATE('5-02-2021 14:00', 'DD-MM-YYYY HH24:MI'),
  91.20,
  TO_DATE('03-02-2020', 'DD-MM-YYYY'),
  TO_DATE('10-02-2020', 'DD-MM-YYYY'),
  12, 
  2,
  17,
  1,
  4
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 50
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Treinamento para se tornar um X-men',
  TO_DATE('30-05-2021 15:00', 'DD-MM-YYYY HH24:MI'),
  16432.90,
  TO_DATE('03-02-2020', 'DD-MM-YYYY'),
  TO_DATE('10-02-2020', 'DD-MM-YYYY'),
  9, 
  2,
  17,
  1,
  28
);