--TIPOS_PAGAMENTOS
insert into tipos_pagamentos (tpa_tipo) VALUES ('DEBITO');
insert into tipos_pagamentos (tpa_tipo) VALUES ('CREDITO');
insert into tipos_pagamentos (tpa_tipo) VALUES ('DINHEIRO');

--TIPOS_EVENTOS
insert into tipos_eventos (tev_descricao) VALUES ('SHOW');
insert into tipos_eventos (tev_descricao) VALUES ('STAND-UP');
insert into tipos_eventos (tev_descricao) VALUES ('CASAMENTO');
insert into tipos_eventos (tev_descricao) VALUES ('ANIVERSÁRIO');
insert into tipos_eventos (tev_descricao) VALUES ('PALESTRA');
insert into tipos_eventos (tev_descricao) VALUES ('APRESENTAÇÃO');
insert into tipos_eventos (tev_descricao) VALUES ('COLAÇÃO DE GRAU');
insert into tipos_eventos (tev_descricao) VALUES ('EVENTOS RELIGIOSOS');
insert into tipos_eventos (tev_descricao) VALUES ('TREINAMENTO');
insert into tipos_eventos (tev_descricao) VALUES ('ENCONTRO ESPIRITUAL');
insert into tipos_eventos (tev_descricao) VALUES ('CONFERÊNCIA');
insert into tipos_eventos (tev_descricao) VALUES ('HAPPY HOUR');
insert into tipos_eventos (tev_descricao) VALUES ('FORMATURA');

--STATUS_AGENDAMENTO
insert into status_agendamento (sta_status) VALUES ('AGENDADO');
insert into status_agendamento (sta_status) VALUES ('PENDETE');
insert into status_agendamento (sta_status) VALUES ('CONCLUIDO');
insert into status_agendamento (sta_status) VALUES ('CANCELADO');
insert into status_agendamento (sta_status) VALUES ('AGUARDANDO PAGAMENTO');

--RESPONSAVEIS
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Wylma', 'Akess', '(20)6742-6896', '(37)79935-1634', 'wakess0@wordpress.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Alexine', 'Skepper', '(47)8518-6727', '(81)80573-7871', 'askepper1@archive.org');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Babbie', 'Seabon', '(85)9252-4376', '(19)84809-4419', 'bseabon2@twitpic.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Crichton', 'Mayling', '(69)3641-1127', '(97)02709-8121', 'cmayling3@mapy.cz');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Antonetta', 'Conn', '(48)9034-3347', '(88)99018-0562', 'aconn4@discovery.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Carrol', 'Pettman', '(13)7180-5977', '(25)30692-5449', 'cpettman5@a8.net');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Avery', 'Mesant', '(31)9334-1299', '(17)92702-4560', 'amesant6@mapquest.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Eddy', 'D''Acth', '(48)9550-3597', '(76)16129-1528', 'edacth7@altervista.org');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Trip', 'Brumbie', '(21)2612-4886', '(47)82164-7550', 'tbrumbie8@hc360.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Fredia', 'Osgarby', '(86)8826-0234', '(85)05988-0409', 'fosgarby9@infoseek.co.jp');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Arlen', 'Valentinuzzi', '(08)6009-0491', '(27)69949-5482', 'avalentinuzzia@mtv.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Nefen', 'Eteen', '(39)8669-4185', '(08)70000-2906', 'neteenb@intel.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Valery', 'Stonebridge', '(12)8179-6918', '(42)91900-8096', 'vstonebridgec@blogspot.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Lester', 'Briatt', '(08)3980-6879', '(56)09399-3311', 'lbriattd@bloglines.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Drake', 'Aslum', '(51)3209-0659', '(36)72516-2395', 'daslume@hostgator.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Corny', 'Tilbey', '(50)4438-1904', '(91)97981-9610', 'ctilbeyf@shutterfly.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Aleda', 'Boothman', '(93)6994-7946', '(80)21387-6950', 'aboothmang@uiuc.edu');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Row', 'Astling', '(57)0266-7482', '(22)17278-1745', 'rastlingh@ning.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Valery', 'Bouchier', '(11)9348-5627', '(78)90431-0688', 'vbouchieri@ebay.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Carlynne', 'Yesinov', '(28)5365-3643', '(59)63291-1940', 'cyesinovj@state.tx.us');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Germana', 'Padbury', '(30)1791-6522', '(47)08937-0748', 'gpadburyk@archive.org');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Yale', 'Ivimey', '(68)7226-3948', '(65)69733-3802', 'yivimeyl@mit.edu');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Katha', 'Gawkroge', '(22)3301-6977', '(45)49079-8180', 'kgawkrogem@unc.edu');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Kienan', 'Waszczyk', '(61)4361-8143', '(25)90693-5471', 'kwaszczykn@vk.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Cello', 'Attkins', '(48)2958-0789', '(25)07083-0329', 'cattkinso@youku.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Harmon', 'Whetland', '(95)4570-0878', '(77)41835-7415', 'hwhetlandp@so-net.ne.jp');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Cristobal', 'Gun', '(49)4588-4701', '(77)83944-4295', 'cgunq@xinhuanet.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Garik', 'Bellows', '(93)5795-6791', '(01)16323-5740', 'gbellowsr@economist.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Kayne', 'Huddlestone', '(63)7514-8778', '(39)82778-8474', 'khuddlestones@youku.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Annabell', 'Gyrgorwicx', '(99)8137-6107', '(85)32566-5159', 'agyrgorwicxt@who.int');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Raven', 'Cancott', '(95)1408-1158', '(22)88419-8065', 'rcancottu@storify.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Tobin', 'Faulder', '(76)7540-0947', '(34)51965-6926', 'tfaulderv@yale.edu');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Lanae', 'Sturch', '(31)9677-3852', '(45)65014-7157', 'lsturchw@ow.ly');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Ardene', 'Vanderplas', '(14)0946-7840', '(72)44660-2138', 'avanderplasx@wp.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Aldin', 'Mobbs', '(85)7794-1482', '(26)21193-2049', 'amobbsy@salon.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Gilberta', 'Garroway', '(02)9240-9662', '(87)62441-7068', 'ggarrowayz@redcross.org');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Warren', 'Atthow', '(43)2664-1647', '(14)95164-4720', 'watthow10@shutterfly.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Jeannette', 'Waistell', '(32)0368-0516', '(37)09278-3694', 'jwaistell11@intel.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Carol', 'Guage', '(98)1149-8140', '(00)62752-1549', 'cguage12@businessweek.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Jozef', 'Curro', '(85)3813-9071', '(00)36940-6416', 'jcurro13@ameblo.jp');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Amelia', 'Manifould', '(86)7268-1919', '(54)01763-2957', 'amanifould14@dedecms.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Jerrie', 'Jack', '(09)0336-0594', '(60)19705-5692', 'jjack15@howstuffworks.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Angie', 'Dryden', '(92)4720-9573', '(85)01333-9260', 'adryden16@skyrock.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Sacha', 'Hoofe', '(70)6787-7660', '(06)79992-9574', 'shoofe17@ihg.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Prudy', 'Alekseicik', '(76)5957-0979', '(85)89146-1714', 'palekseicik18@tiny.cc');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Analise', 'Sawrey', '(15)4762-2671', '(00)18512-8934', 'asawrey19@meetup.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Alexia', 'Rosgen', '(53)5626-8755', '(96)81645-3182', 'arosgen1a@booking.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Doug', 'Walentynowicz', '(21)3175-5788', '(93)58475-6870', 'dwalentynowicz1b@nsw.gov.au');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Beauregard', 'Pickvance', '(27)5207-1933', '(70)99083-3947', 'bpickvance1c@mashable.com');
insert into responsaveis (res_nome, res_sobrenome, res_telefone_fixo, res_telefone_movel, res_email) values ('Aldric', 'Bon', '(14)0145-8843', '(51)25265-6694', 'abon1d@lulu.com');

--LOCAIS_EVENTO
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3360', 'Zheleznogorsk', 'Shoshone', '26875-056', 'PI', 'Way');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7280', 'Xinhuang', 'Dryden', '93945-850', 'GO', 'Point');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6467', 'Ḩarastā', '1st', '31966-976', 'CE', 'Center');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2070', 'Embalse', 'Larry', '20154-577', 'RO', 'Hill');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2995', 'Jiupu', 'Beilfuss', '35315-884', 'PR', 'Trail');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('8953', 'Bauchi', 'Sheridan', '07546-721', 'MA', 'Junction');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2346', 'Fort Dauphin', 'Village', '97232-130', 'ES', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3738', 'Berezne', 'Trailsway', '86927-563', 'RJ', 'Trail');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2073', 'Chambishi', 'Golden Leaf', '38719-079', 'MT', 'Hill');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('5929', 'Gustavia', 'Pierstorff', '03847-839', 'PA', 'Alley');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6404', 'Chaoyangdong', 'Morning', '30733-037', 'MG', 'Terrace');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6398', 'Cihaladan', 'Park Meadow', '10603-194', 'GO', 'Street');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7353', 'Odintsovo', 'Surrey', '46072-424', 'TO', 'Pass');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7165', 'Cidade Velha', 'Fieldstone', '31315-109', 'MS', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('5733', 'Kalá Déndra', 'Granby', '30484-499', 'RO', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7206', 'Xinmatou', 'Boyd', '31313-430', 'GO', 'Park');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('5589', 'Fengle', 'Brown', '38181-253', 'RS', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('8126', 'Prusice', 'Mifflin', '98880-253', 'RR', 'Park');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('9699', 'Korogan Timur', 'Carpenter', '57971-616', ' AL', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('9729', 'San Cristóbal', 'Blue Bill Park', '67640-261', 'PI', 'Crossing');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('0173', 'Jiangkou', 'Ridgeway', '48799-121', ' AC', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('0578', 'Yonezawa', 'Susan', '63948-569', ' AC', 'Center');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7993', 'Lanot', 'Chinook', '08302-856', 'AM', 'Hill');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3037', 'Alcains', 'Nevada', '40778-104', 'MT', 'Way');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7140', 'São Gonçalo do Amarante', 'Commercial', '62204-742', 'MA', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7260', 'Sandefjord', 'Forest', '18628-179', 'PB', 'Terrace');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7961', 'West End', 'Prentice', '97867-543', 'GO', 'Circle');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('5279', 'Moengo', 'Straubel', '04149-049', 'ES', 'Drive');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2498', 'El Matama', 'Petterle', '99418-810', 'PR', 'Circle');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('9467', 'Péfki', 'Hauk', '08363-111', 'ES', 'Hill');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6155', 'Huangdimiao', 'Kennedy', '62198-412', 'MT', 'Alley');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('0954', 'Sambiyan', 'Orin', '40818-093', 'PE', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6176', 'Wanmao', 'Stephen', '81198-747', ' AC', 'Court');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('8064', 'Lerrnanist', 'Morningstar', '46980-341', 'PR', 'Court');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('0152', 'Merritt', 'Algoma', '30581-701', 'MA', 'Junction');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3689', 'Dapaong', 'Northview', '10803-832', 'RR', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('5923', 'Gielniów', 'Del Sol', '89049-783', 'MS', 'Junction');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('4592', 'Valuyki', 'Buhler', '60633-303', 'ES', 'Drive');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7696', 'Asen', 'Prairie Rose', '35376-553', 'TO', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6504', 'Rouen', 'Westridge', '64504-628', 'PB', 'Court');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('1079', 'Aţ Ţaybah', 'Leroy', '47057-750', 'BA', 'Junction');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3325', 'Chang Klang', 'Claremont', '02756-280', 'MT', 'Point');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('1733', 'Gouménissa', 'Anniversary', '24216-532', 'PE', 'Drive');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('2333', 'Cane', 'Columbus', '94301-567', 'RJ', 'Hill');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('8683', 'Margahayu', 'Summer Ridge', '55699-496', 'SC', 'Road');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7862', 'Mwene-Ditu', 'Debra', '57054-294', 'AM', 'Trail');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('6223', 'Bukowina Tatrzańska', 'Lukken', '10882-793', 'PR', 'Lane');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('3157', 'La Esmeralda', 'Bonner', '94013-414', 'ES', 'Street');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('7223', 'San Kamphaeng', 'Basil', '93810-549', 'MS', 'Plaza');
insert into locais_eventos (loc_numero, loc_cidade, loc_logradouro, loc_cep, loc_estado, loc_bairro) values ('1673', 'Firovo', 'Grayhawk', '47653-501', 'ES', 'Trail');

--- *** EVENTOS ***
insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 4
  eve_sta_id,-- 1 a 4
  eve_res_id,-- 1 a 100
  eve_tpa_id, -- 1 a 2
  eve_loc_id
) values (
  'Show do Truman',
  TO_DATE('12-02-2020', 'DD-MM-YYYY HH24:MI'),
  7566.34,
  TO_DATE('12-02-2020', 'DD-MM-YYYY'),
  TO_DATE('12-02-2020', 'DD-MM-YYYY'),
  2, 
  1,
  3,
  1,
  1
);


insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 4
  eve_sta_id,-- 1 a 4
  eve_res_id,-- 1 a 100
  eve_tpa_id, -- 1 a 2
  eve_loc_id
) values (
  'Meetup do Nerdzão',
  TO_DATE('12-11-2020 23:55', 'DD-MM-YYYY HH24:MI'),
  36880.87,
  TO_DATE('12-11-2020', 'DD-MM-YYYY'),
  TO_DATE('12-12-2020', 'DD-MM-YYYY'),
  3, 
  1,
  3,
  1,
  1
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 4
  eve_sta_id,-- 1 a 4
  eve_res_id,-- 1 a 100
  eve_tpa_id, -- 1 a 2
  eve_loc_id
) values (
  'Meetup do Caquicoders',
  TO_DATE('08-09-2020 08:35', 'DD-MM-YYYY HH24:MI'),
  2737.65,
  TO_DATE('10-09-2020', 'DD-MM-YYYY'),
  TO_DATE('12-10-2020', 'DD-MM-YYYY'),
  6, 
  1,
  3,
  1,
  1
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 100
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Show Aerosmith',
  TO_DATE('30-10-2020 19:30', 'DD-MM-YYYY HH24:MI'),
  7642.56,
  TO_DATE('10-10-2020', 'DD-MM-YYYY'),
  TO_DATE('16-10-2020', 'DD-MM-YYYY'),
  1, 
  4,
  45,
  1,
  33
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 50
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Teatro do Fabio Porchat',
  TO_DATE('20-11-2020 20:30', 'DD-MM-YYYY HH24:MI'),
  1427.76,
  TO_DATE('12-11-2020', 'DD-MM-YYYY'),
  TO_DATE('13-12-2020', 'DD-MM-YYYY'),
  2, 
  1,
  6,
  2,
  23
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 50
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Casamento do Robinho',
  TO_DATE('25-12-2020 16:30', 'DD-MM-YYYY HH24:MI'),
  4667.76,
  TO_DATE('05-11-2020', 'DD-MM-YYYY'),
  TO_DATE('10-11-2020', 'DD-MM-YYYY'),
  3, 
  3,
  50,
  3,
  12
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 50
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Festa do Latino',
  TO_DATE('30-02-2021 22:00', 'DD-MM-YYYY HH24:MI'),
  13090.76,
  TO_DATE('10-01-2020', 'DD-MM-YYYY'),
  TO_DATE('30-01-2020', 'DD-MM-YYYY'),
  4, 
  4,
  26,
  2,
  8
);


insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 50
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Fim de semana no parque',
  TO_DATE('5-02-2021 14:00', 'DD-MM-YYYY HH24:MI'),
  91.20,
  TO_DATE('03-02-2020', 'DD-MM-YYYY'),
  TO_DATE('10-02-2020', 'DD-MM-YYYY'),
  12, 
  2,
  17,
  1,
  4
);

insert into eventos(
  eve_nome,
  eve_data,
  eve_valor,
  eve_data_pagamento,
  eve_data_vencimento,
  eve_tev_id,-- 1 a 13
  eve_sta_id,-- 1 a 5
  eve_res_id,-- 1 a 50
  eve_tpa_id, -- 1 a 3
  eve_loc_id -- 1 a 100
) values (
  'Treinamento para se tornar um X-men',
  TO_DATE('30-05-2021 15:00', 'DD-MM-YYYY HH24:MI'),
  16432.90,
  TO_DATE('03-02-2020', 'DD-MM-YYYY'),
  TO_DATE('10-02-2020', 'DD-MM-YYYY'),
  9, 
  2,
  17,
  1,
  28
);

